"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Recipe extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Recipe.init(
    {
      recipe_id: {
        type: DataTypes.INTEGER,
      },
      description: { type: DataTypes.STRING },
      status: { type: DataTypes.INTEGER },
      step_order: { type: DataTypes.INTEGER },
    },
    {
      sequelize,
      modelName: "Recipe",
    }
  );
  return Recipe;
};
