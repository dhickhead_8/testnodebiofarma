require("dotenv").config();
const express = require("express");
const { Sequelize } = require("sequelize");
const app = express();

// Routes

const sequelize = new Sequelize(
  process.env.MYSQL_DBNAME,
  process.env.MYSQL_USER,
  "",
  {
    host: process.env.MYSQL_HOST,
    dialect: "mysql",
  }
);

(async () => {
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");

    const app = express();
    app.use(express.json());
    app.use("/api/auth", require("./routes/api/auth"));
    app.use("/api/recipeCategory", require("./routes/api/recipeCategory"));
    //console.log(process.env);
    app.listen(process.env.NODE_DOCKER_PORT || 3030, () =>
      console.log("server started at port " + process.env.NODE_DOCKER_PORT)
    );
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
})();
