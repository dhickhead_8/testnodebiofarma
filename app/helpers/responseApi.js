/**
 * @desc    This file contain Success and Error response for sending to client / user
 * @author  Dikara Derandia
 * @since   2022
 */

const res = require("express/lib/response");

/**
 * @desc    Send any success response
 *
 * @param   {string} message
 * @param   {object | array} results
 * @param   {number} statusCode
 */
exports.success = (message, results, statusCode) => {
  return {
    message,
    success: true,
    data: results,
  };
};

/**
 * @desc    Send any error response
 *
 * @param   {string} message
 * @param   {number} statusCode
 */
exports.error = (message) => {
  return {
    message,
    success: false,
  };
};

/**
 * @desc    Send any validation response
 *
 * @param   {object | array} errors
 */
exports.validation = (errors) => {
  return {
    message: "error validation!",
    success: false,
    data: errors,
  };
};
