const { success, error, validation } = require("../../helpers/responseApi");
const { randomString } = require("../../helpers/common");
const { validationResult } = require("express-validator");
const config = require("config");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const { User, sequelize } = require("../../../models");

/**
 * @desc    Register a new user
 * @method  POST api/auth/register
 * @access  public
 */
exports.registerSequelize = async (req, res) => {
  // Validation
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(422).json(validation(errors.array()));

  const { name, email, password } = req.body;

  let transaction;
  try {
    transaction = await sequelize.transaction();
    let user = await User.findOne({ where: { email: email.toLowerCase() } });

    // Check the user email
    if (user)
      return res
        .status(422)
        .json(validation({ msg: "Email already registered" }));
    let newUser = await User.create(
      {
        name,
        email: email.toLowerCase().replace(/\s+/, ""),
        password,
      },
      { transaction }
    );

    // Hash the password
    const hash = await bcrypt.genSalt(10);
    newUser.password = await bcrypt.hash(password, hash);

    // Save the user
    await newUser.save({ transaction });
    await transaction.commit();

    // Send the response
    res.status(201).json(
      success("Register success, please activate your account.", {
        user: {
          id: newUser._id,
          name: newUser.name,
          email: newUser.email,
          createdAt: newUser.createdAt,
        },
      })
    );
  } catch (err) {
    if (transaction) await transaction.rollback();

    console.error(err.message);
    res.status(500).json(error("Server error"));
  }
};

/**
 * @desc    Login a user
 * @method  POST api/auth/login
 * @access  public
 */
exports.login = async (req, res) => {
  // Validation
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(422).json(validation(errors.array()));

  const { email, password } = req.body;

  try {
    const user = await User.findOne({ where: { email: email.toLowerCase() } });

    // Check the email
    // If there's not exists
    // Throw the error
    if (!user) return res.status(422).json(validation("Invalid credentials"));

    // Check the password
    let checkPassword = await bcrypt.compare(password, user.password);
    if (!checkPassword)
      return res.status(422).json(validation("Invalid credentials"));

    // If the requirement above pass
    // Lets send the response with JWT token in it
    const payload = {
      user: {
        id: user.id,
        name: user.name,
        email: user.email,
      },
    };

    jwt.sign(
      payload,
      process.env.JWTSECRET,
      { expiresIn: 3600 },
      (err, token) => {
        if (err) throw err;

        res.status(200).json(success("Login success", { token }));
      }
    );
  } catch (err) {
    console.log(err.message);
    res.status(500).json(error("Server error"));
  }
};

/**
 * @desc    Get authenticated user
 * @method  GET api/auth
 * @access  private
 */
exports.getAuthenticatedUser = async (req, res) => {
  return res.json(req.user);
};
