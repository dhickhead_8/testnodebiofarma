const { success, error, validation } = require("../../helpers/responseApi");
const { randomString } = require("../../helpers/common");
const { validationResult } = require("express-validator");
const config = require("config");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const { RecipeCategory, sequelize } = require("../../../models");

/**
 * @desc    Register a new user
 * @method  POST api/auth/register
 * @access  public
 */
exports.createRecipeCategory = async (req, res) => {
  // Validation
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(422).json(validation(errors.array()));

  const { name } = req.body;

  let transaction;
  try {
    transaction = await sequelize.transaction();
    let user = await RecipeCategory.findOne({
      where: { name: name.toLowerCase() },
    });

    // Check the user email
    if (user)
      return res.status(422).json(validation({ msg: "Name already Existed" }));
    let recipe = await RecipeCategory.create(
      {
        name,
      },
      { transaction }
    );

    await transaction.commit();

    // Send the response
    res.status(201).json(success("Create recipe success.", recipe));
  } catch (err) {
    if (transaction) await transaction.rollback();
    console.error(err.message);
  }
};

/**
 * @desc    Register a new user
 * @method  POST api/auth/register
 * @access  public
 */
exports.updateRecipeCategory = async (req, res) => {
  // Validation
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(422).json(validation(errors.array()));
  const { name } = req.body;
  const { id } = req.params;

  let transaction;
  try {
    transaction = await sequelize.transaction();
    let recipe = await RecipeCategory.findByPk(id);

    // Check the user email
    if (!recipe)
      return res.status(404).json(error("recipe category not found"));

    recipe.name = name;
    await recipe.save({ transaction });
    await transaction.commit();

    // Send the response
    res.status(201).json(success("recipe update success.", recipe));
  } catch (err) {
    if (transaction) await transaction.rollback();
    console.error(err.message);
  }
};

exports.deleteRecipeCategory = async (req, res) => {
  // Validation
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(422).json(validation(errors.array()));
  const { name } = req.body;
  const { id } = req.params;

  let transaction;
  try {
    transaction = await sequelize.transaction();
    let recipe = await RecipeCategory.findByPk(id);

    // Check the user email
    if (!recipe)
      return res.status(404).json(error("recipe category not found"));
    await recipe.destroy({ transaction });
    await transaction.commit();

    // Send the response
    res.status(200).json(success("recipe delete success.", {}));
  } catch (err) {
    if (transaction) await transaction.rollback();
    console.error(err.message);
  }
};

exports.getList = async (req, res) => {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    let recipe = await RecipeCategory.findAll();

    // Check the user email
    if (!recipe)
      return res.status(404).json(error("recipe category not found"));
    // Send the response
    res.status(200).json(success("recipe find.", recipe));
  } catch (err) {
    if (transaction) await transaction.rollback();
    console.error(err.message);
  }
};
