const router = require("express").Router();

// Controllers
const {
  createRecipeCategory,
  updateRecipeCategory,
  deleteRecipeCategory,
  getList,
} = require("../../app/controllers/api/RecipeCategoryController");

const { auth } = require("../../app/middlewares/auth");

// Middleware
const {
  createUpdateRecipeCategoryValidation,
} = require("../../app/middlewares/recipeCategory");

// Routes
router.post(
  "/create",
  auth,
  createUpdateRecipeCategoryValidation,
  createRecipeCategory
);

router.put(
  "/update/:id",
  auth,
  createUpdateRecipeCategoryValidation,
  updateRecipeCategory
);

router.delete("/delete/:id", auth, deleteRecipeCategory);

router.put("/update/:id", auth, updateRecipeCategory);

router.get("/", auth, getList);

module.exports = router;
