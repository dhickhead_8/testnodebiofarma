const router = require("express").Router();

// Controllers
const {
  registerSequelize,
  login,
  getAuthenticatedUser,
} = require("../../app/controllers/api/AuthController");

// Middleware
const {
  registerValidation,
  loginValidation,
  auth,
} = require("../../app/middlewares/auth");

// Routes
router.post("/register", registerValidation, registerSequelize);
router.post("/login", loginValidation, login);
router.get("/me", auth, getAuthenticatedUser);

module.exports = router;
