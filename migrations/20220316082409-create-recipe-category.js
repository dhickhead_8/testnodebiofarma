"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface
      .createTable("RecipeCategories", {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        name: {
          type: Sequelize.STRING,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
      })
      .then(function () {
        queryInterface.createTable("recipe", {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
          },
          recipe_category_id: {
            type: Sequelize.INTEGER,
            references: { model: "RecipeCategories", key: "id" },
          },
          name: { type: Sequelize.STRING },
          reaction_like: { type: Sequelize.INTEGER },
          reaction_dislike: { type: Sequelize.INTEGER },
          reaction_neutral: { type: Sequelize.INTEGER },
          image: { type: Sequelize.STRING },
        });
      })
      .then(function () {
        queryInterface.createTable("steps", {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
          },
          recipe_id: {
            type: Sequelize.INTEGER,
            references: { model: "recipe", key: "id" },
          },
          description: { type: Sequelize.STRING },
          status: { type: Sequelize.INTEGER },
          step_order: { type: Sequelize.INTEGER },
        });
      });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("RecipeCategories");
  },
};
